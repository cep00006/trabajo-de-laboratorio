public class empleado {

    // Atributos
    private String nombre;
    private int edad;
    private double sueldo;
    private String categoria;
    private String cargo;
    
    // Constructor
    public empleado(String nombre, int edad, double sueldo, String categoria, String cargo) {
        this.nombre = nombre;
        this.edad = edad;
        this.sueldo = sueldo;
        this.categoria = categoria;
        this.cargo = cargo;
    }
    
    
    
    //@Override
    public String toString() {
        return "Empleado [nombre=" + nombre + ", edad=" + edad + ", sueldo=" + sueldo + ", categoria=" + categoria + ", cargo=" + cargo + "]";
    }
    
    
    public static void main(String[] args) {
        // Crear objetos Empleado
        empleado admin1 = new empleado("Juan", 35, 2000.0, "Administrativo", "Secretario");
        
        empleado doctor1 = new empleado("Carlos", 40, 3000.0, "Doctor", "Cardiólogo");
        
        empleado enfermero1 = new empleado("Luis", 28, 1000.0, "Enfermero", "Enfermero");
        
        empleado chofer1 = new empleado("Pedro", 45, 1200.0, "Chofer", "Chofer");
        
        
        // Imprimir detalles de los empleados
        System.out.println(admin1);
        
        System.out.println(doctor1);
        
        System.out.println(enfermero1);
        
        System.out.println(chofer1);
       
        
       
    }
}
